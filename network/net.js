import log from "../logger/index.js";

class Networks {
  constructor() {
    this.partitions = [];
    this.goose = null;
    this.gooseServer = null;
    this.largestPartition = [];
    this.largestPartitionIndex = 0;
    this.previousLargestPartitionIndex = 0;
  }

  resetGoose() {
    this.goose = null;
    this.gooseServer = null;
  }

  getGoose() {
    return this.goose;
  }

  getPartitions() {
    return this.partitions;
  }

  getLargestPartition() {
    return this.largestPartition;
  }

  startGooseServer(port) {
    if (this.goose && !this.gooseServer) {
      log.info(`start listening on server ${this.goose.proxy} on port ${port}`);
      this.gooseServer = this.goose.listen(port);
    } else {
      log.info("goose is not set");
    }
  }

  endGooseServer() {
    log.info(`kill server ${this.goose.proxy}`);
    this.gooseServer.close();
    this.gooseServer = null;
  }

  addToNetworks(network) {
    this.partitions.push(network);
    // if new partition is a tie or smaller than the current largest partition, we do nothing
    this.findLargestPartition();
    // if there is a goose and the goose belongs in the largest partition we do not need a new election
    this.election();

    // this.election();
  }

  findLargestPartition() {
    // console.log("this.partitions", this.partitions);
    const partitions = this.partitions;
    // at this index the partition is the largest so we can reach a quorum
    let largestPartitionIndex = this.largestPartitionIndex;
    for (let i = 0; i < partitions.length; i++) {
      // find the partition with the largest amount of network nodes in it
      const partition = partitions[i];
      if (partition.length > this.largestPartition.length) {
        largestPartitionIndex = i;
      }
    }
    this.previousLargestPartitionIndex = this.largestPartitionIndex;
    this.largestPartition = this.partitions[largestPartitionIndex];
    this.largestPartitionIndex = largestPartitionIndex;
  }

  election() {
    if (
      !this.goose ||
      this.previousLargestPartitionIndex != this.largestPartitionIndex
    ) {
      // check if goose is running
      if (this.gooseServer) {
        this.endGooseServer();
      }
      // the elected node will be the goose
      const electedNode =
        this.largestPartition[
          Math.floor(Math.random() * this.largestPartition.length)
        ];
      this.goose = electedNode;
      // after every election we start the goose sever
      this.startGooseServer(8080);
    }
    log.info({
      goose: this.goose.proxy,
    });
  }
}

export default Networks;
