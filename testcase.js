import Networks from "./network/net.js";
import Node from "./nodes/node.js";

// init the entire system
const Network = new Networks();
// create instance of network of nodes
const NodeOne = new Node();
const NodeTwo = new Node();
NodeOne.initServers(3);
NodeTwo.initServers(2);
const serverOne = NodeOne.getNodes();
const serverTwo = NodeTwo.getNodes();
// add instances to the system
Network.addToNetworks(serverTwo);
Network.addToNetworks(serverOne);

const goose = Network.getGoose();

// case to delete a node
// find a random node in network of nodes
const randServer = NodeOne.getRandomNode();
NodeOne.deleteNode(randServer);
// if we delete the node that is the goose...
if (randServer.proxy === goose.proxy) {
  // end the current goose server
  Network.endGooseServer();
  Network.resetGoose();
}
// regardless if the node is the goose find if deleted node affects the ability to reach quorums
Network.findLargestPartition();
Network.election();
const secondGoose = Network.getGoose();

// add new instance that will not affect the system and not trigger an election
const NodeThree = new Node();
NodeThree.initServers(1);
const serverThree = NodeThree.getNodes();
Network.addToNetworks(serverThree);
const goose3 = Network.getGoose();
