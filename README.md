# DuckDuckGoose

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/dimsum1/sleeper.git
git branch -M main
git push -uf origin main
```

## Name

DuckDuckGoose

## Description

A high level recreation of a distributed system where one node is the goose if a collection of nodes if there is a majority in the holistic network.

## Usage

Run this application by running `yarn dev` \
Test the application by running `yarn test`

## Future Improvements

There are a number of things we can do to optimize this build such as:

- We should not have to find the largest partition after every delete or add of a new network of nodes into the system if we can keep track of the largest partition.
- We can potentially merge Systems with one another
- We can use a better id per every app to keep track of the nodes id
