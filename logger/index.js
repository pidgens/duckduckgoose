import bunyan from "bunyan";

const log = bunyan.createLogger({ name: "duckduckgoose" });

export default log;
