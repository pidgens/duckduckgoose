// const http = require('http')
// const koa = require('koa');
// const koaRouter = require('koa-router');

import koa from "koa";
import koaRouter from "koa-router";
import uuid4 from "uuid4";

const setupApp = () => {
  const app = new koa();
  const router = new koaRouter();

  router.get("/check", async (ctx) => {
    ctx.body = "quack but im a goose";
  });

  router.get("/health_check", (ctx) => {
    ctx.body = "OK";
  });

  app.use(router.routes()).use(router.allowedMethods());
  // set this as a proxy to differentiate the apps
  app.proxy = uuid4();
  return app;
};
export default setupApp;
