import Networks from "./network/net.js";
import Node from "./nodes/node.js";

// init the entire system
const Network = new Networks();
// create instance of network of nodes
const NodeOne = new Node();
const NodeTwo = new Node();
NodeOne.initServers(3);
NodeTwo.initServers(2);
const serverOne = NodeOne.getNodes();
const serverTwo = NodeTwo.getNodes();
Network.addToNetworks(serverOne);
Network.addToNetworks(serverTwo);
