import setupApp from "./../app.js";
import log from "./../logger/index.js";

class Nodes {
  constructor() {
    this.servers = [];
  }

  getNodes() {
    return this.servers;
  }

  getRandomNode() {
    return this.servers[Math.floor(Math.random() * this.servers.length)];
  }

  initServers(numberOfServers) {
    for (let i = 0; i < numberOfServers; i++) {
      const app = setupApp();
      // serverMap[i] = app;
      this.servers.push(app);
    }
  }

  deleteNode(node) {
    for (let i = 0; i < this.servers.length; i++) {
      const app = this.servers[i];
      if (app.proxy === node.proxy) {
        this.servers.splice(i, 1);
      }
    }
    log.info("Deleted from Node object");
  }
}

export default Nodes;
