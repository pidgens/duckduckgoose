import Networks from "../network/net.js";
import Node from "../nodes/node.js";

describe("test my duck", () => {
  let Network;
  beforeAll(() => {
    Network = new Networks();
  });

  afterAll(() => {
    const goose = Network.getGoose();
    if (goose) {
      Network.endGooseServer();
    }
  });

  test("test getNodes", () => {
    const NodeOne = new Node();
    NodeOne.initServers(3);
    const serverOne = NodeOne.getNodes();
    expect(serverOne.length).toBe(3);
  });

  test("goose to change if a new partition is added larger than initial one", () => {
    const NodeOne = new Node();
    NodeOne.initServers(1);
    const serverOne = NodeOne.getNodes();
    Network.addToNetworks(serverOne);
    const firstGoose = Network.getGoose();
    const NodeTwo = new Node();
    NodeTwo.initServers(2);
    const serverTwo = NodeTwo.getNodes();
    Network.addToNetworks(serverTwo);
    const secondGoose = Network.getGoose();
    expect(firstGoose.proxy).not.toBe(secondGoose.proxy);
  });

  test("goose reassigned after deletes", () => {
    const NodeOne = new Node();
    NodeOne.initServers(1);
    const serverOne = NodeOne.getNodes();
    Network.addToNetworks(serverOne);
    const gooseOne = Network.getGoose();
    const NodeTwo = new Node();
    NodeTwo.initServers(2);
    const randOne = NodeTwo.getRandomNode();
    NodeTwo.deleteNode(randOne);
    const randTwo = NodeTwo.getRandomNode();
    NodeTwo.deleteNode(randTwo);
    const gooseTwo = Network.getGoose();
    expect(gooseOne.proxy).toBe(gooseTwo.proxy);
  });
});
